from django.urls import path
from .views import HomeView, Contact

urlpatterns = [
    path('', HomeView.as_view(), name='index'),
    path('contact/', Contact.as_view(), name='contact'),
]