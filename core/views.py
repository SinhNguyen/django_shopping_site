from django.shortcuts import render
from django.views import View

class HomeView(View):
    def get(self, request):
        return render(request, 'homepage/index.html')

class Contact(View):
    def get(self, request):
        return render(request, 'homepage/contact.html')
